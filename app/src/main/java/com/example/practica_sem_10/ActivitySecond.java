package com.example.practica_sem_10;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.practica_sem_10.adapters.pokemonAdapter;
import com.example.practica_sem_10.entities.pokemon;
import com.example.practica_sem_10.factories.RetrofitFactory;
import com.example.practica_sem_10.services.PokemonService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivitySecond extends AppCompatActivity {

    List<pokemon> pokemons = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Retrofit retrofit = RetrofitFactory.build();
        PokemonService service = retrofit.create(PokemonService.class);

        Call<List<pokemon>> call = service.getContacts();

        call.enqueue(new Callback<List<pokemon>>() {
            @Override
            public void onResponse(Call<List<pokemon>> call, Response<List<pokemon>> response) {
                if(!response.isSuccessful()) {
                    Log.e("APP_VJ20202", "Error de aplicación");
                } else {
                    Log.i("APP_VJ20202", "Respuesta Correcta");
                    pokemons = response.body();

                    pokemonAdapter adapter = new pokemonAdapter(pokemons);

                    RecyclerView rv = findViewById(R.id.rvPokemons);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    rv.setHasFixedSize(true);
                    rv.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<pokemon>> call, Throwable t) {
                Log.e("APP_VJ20202", "No hubo conectividad con el servicio web");
            }
        });
    }
}