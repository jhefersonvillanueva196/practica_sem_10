package com.example.practica_sem_10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica_sem_10.entities.pokemon;
import com.example.practica_sem_10.factories.RetrofitFactory;
import com.example.practica_sem_10.services.PokemonService;

import javax.security.auth.login.LoginException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityQuarter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quarter);

        Spinner tipos = findViewById(R.id.sTipo);
        ArrayAdapter<CharSequence>adapter= ArrayAdapter.createFromResource(this, R.array.tipos, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);

        tipos.setAdapter(adapter);

        EditText nombre = findViewById(R.id.etNombre);
        Spinner tipo = findViewById(R.id.sTipo);
        EditText region = findViewById(R.id.etRegion);
        EditText codigo = findViewById(R.id.etCodigo);
        Button crear = findViewById(R.id.btCrear);

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pokemon pokemon = new pokemon();
                pokemon.nombre = nombre.getText().toString();
                pokemon.tipo = tipo.getSelectedItem().toString();
                pokemon.region = region.getText().toString();
                pokemon.codigo = codigo.getText().toString();
                if(pokemon.nombre=="Nombre"||pokemon.nombre==""||pokemon.region=="Region"||pokemon.region==""){
                    Toast.makeText(ActivityQuarter.this,"Los campos son obligatorios",Toast.LENGTH_SHORT).show();
                }else{
                    Retrofit retrofit = RetrofitFactory.build();
                    PokemonService service = retrofit.create(PokemonService.class);

                    Call<pokemon> call = service.create(pokemon);

                    call.enqueue(new Callback<pokemon>() {
                        @Override
                        public void onResponse(Call<pokemon> call, Response<pokemon> response) {
                            if (response.isSuccessful()) {
                                Log.i("Crear", "Se creo correctamente");
                                Intent intent = new Intent(ActivityQuarter.this,ActivitySecond.class);
                                startActivity(intent);
                            } else {
                                Log.e("APP_VJ20202", "No se pudo eliminar el contacto");
                            }
                        }

                        @Override
                        public void onFailure(Call<pokemon> call, Throwable t) {
                            Log.e("APP_VJ20202", "No nos podemos conectar al servicio web");
                        }
                    });

                }


            }
        });


    }
}