package com.example.practica_sem_10.entities;

public class pokemon {
    public int id;
    public String nombre;
    public String region;
    public String tipo;
    public String codigo;


    public pokemon() {
    }

    public pokemon(int id, String nombre, String region, String tipo,String codigo) {
        this.id = id;
        this.nombre = nombre;
        this.region = region;
        this.tipo = tipo;
        this.codigo = codigo;
    }
}
