package com.example.practica_sem_10;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.practica_sem_10.entities.pokemon;
import com.example.practica_sem_10.factories.RetrofitFactory;
import com.example.practica_sem_10.services.PokemonService;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityThird extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        String contactJson = getIntent().getStringExtra("POKEMON");
        pokemon pokemon = new Gson().fromJson(contactJson, pokemon.class);

        TextView tvNombre = findViewById(R.id.tvNombre);
        TextView tvTipo = findViewById(R.id.tvTipo);
        TextView tvRegion = findViewById(R.id.tvRegion);
        TextView tvCodigo = findViewById(R.id.tvCodigo);
        Button eliminar = findViewById(R.id.btEliminar);

        tvNombre.setText(pokemon.nombre);
        tvTipo.setText(pokemon.tipo);
        tvRegion.setText(pokemon.region);
        tvCodigo.setText(pokemon.codigo);

        Context context = this;

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = RetrofitFactory.build();
                PokemonService service = retrofit.create(PokemonService.class);

                Call<pokemon> call = service.delete(pokemon.id);

                call.enqueue(new Callback<pokemon>() {
                    @Override
                    public void onResponse(Call<pokemon> call, Response<pokemon> response) {
                        if (response.isSuccessful()) {
                            Log.i("Eliminar", "Se elimino correctamente al contacto " + pokemon.id);
                            Intent intent = new Intent(context,ActivitySecond.class);
                            startActivity(intent);
                        } else {
                            Log.e("APP_VJ20202", "No se pudo eliminar el contacto");
                        }
                    }

                    @Override
                    public void onFailure(Call<pokemon> call, Throwable t) {
                        Log.e("APP_VJ20202", "No nos podemos conectar al servicio web");
                    }
                });
            }
        });

    }
}