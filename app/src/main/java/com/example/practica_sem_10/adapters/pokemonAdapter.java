package com.example.practica_sem_10.adapters;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practica_sem_10.ActivityThird;
import com.example.practica_sem_10.R;
import com.example.practica_sem_10.entities.pokemon;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

public class pokemonAdapter extends RecyclerView.Adapter<pokemonAdapter.pokemonViewHolder>{

    List<pokemon> pokemons;
    public pokemonAdapter(List<pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    @NonNull
    @Override
    public pokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
        return new pokemonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull pokemonViewHolder vh, int position) {

        View itemView = vh.itemView;

        pokemon pokemon = pokemons.get(position);
        ImageView image = itemView.findViewById(R.id.image);
        TextView tvId = itemView.findViewById(R.id.tvCodigo);
        TextView tvNombre = itemView.findViewById(R.id.tvNombre);

        String path = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/"+
                pokemon.codigo + ".svg";
        Log.i("Imagen", path);
        Picasso.get().load(path).fit().into(image);
        tvId.setText("Codigo: " + pokemon.codigo);
        tvNombre.setText("Nombre: " + pokemon.nombre);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), ActivityThird.class);

                String contactJSON = new Gson().toJson(pokemon);
                intent.putExtra("POKEMON", contactJSON);

                itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    class pokemonViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {

        public pokemonViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            pokemon contact = pokemons.get(i);
            Log.i("APP_VJ20202", "click en el elemento" + contact.id);
        }
    }
}
