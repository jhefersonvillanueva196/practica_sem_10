package com.example.practica_sem_10.services;

import com.example.practica_sem_10.entities.pokemon;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PokemonService {

    @GET("pokemon")
    Call<List<pokemon>> getContacts();

    // contacts/:id
    @GET("pokemon/{id}")
    Call<pokemon> findContact(@Path("id") int id);

    @POST("pokemon")
    Call<pokemon> create(@Body pokemon pokemon);

    @DELETE("pokemon/{id}")
    Call<pokemon> delete(@Path("id") int id);

}
